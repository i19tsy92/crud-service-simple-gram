package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.crud.model.Client;

import java.util.UUID;

@Repository
public interface ClientRepo extends ReactiveMongoRepository<Client, UUID> {
}
