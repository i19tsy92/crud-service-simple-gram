package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.user.User;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, UUID> {

    Optional<Mono<User>> findUserByUsernameAndHashPassword(String username, String hashPassword);

    Mono<User> findUserByUsername(String username);
}
