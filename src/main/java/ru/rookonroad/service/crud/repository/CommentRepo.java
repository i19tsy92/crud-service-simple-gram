package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import ru.rookonroad.service.crud.model.Comment;

@Repository
public interface CommentRepo extends ReactiveMongoRepository<Comment, Long> {

    Flux<Comment> findAllByImageCard(String cardId);
}
