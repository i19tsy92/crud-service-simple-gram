package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.crud.model.Image;

@Repository
public interface ImageRepo extends ReactiveMongoRepository<Image, String> {
}
