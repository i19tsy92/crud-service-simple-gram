package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.security.Session;

import java.util.UUID;

@Repository
public interface SessionRepo extends ReactiveMongoRepository<Session, String> {

    Mono<Session> findSessionByUserId(UUID id);
    Mono<Session> findSessionByTokens_RefreshToken(String refreshToken);
    Mono<Session> findSessionByTokens_AccessToken(String accessToken);
}
