package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import ru.rookonroad.service.crud.model.ImageCard;

@Repository
public interface CardRepo extends ReactiveMongoRepository<ImageCard, String> {
}
