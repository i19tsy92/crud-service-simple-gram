package ru.rookonroad.service.crud.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import reactor.core.publisher.Flux;
import ru.rookonroad.service.crud.model.Message;

public interface ChatRepository extends ReactiveMongoRepository<Message, String> {

    @Tailable
    Flux<Message> findWithTailtableCursorByChannelId(String channelId);
}
