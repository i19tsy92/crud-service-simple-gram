package ru.rookonroad.service.crud.utils;

import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

public class TokensUtils {

    public static String key = "qwwerty123";

    public static String buildAccessToken(UUID userId, LocalDateTime dateTime) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(key.getBytes(), "RawBytes"));
            mac.update(DigestUtils.sha512(userId.toString()));
            mac.update(DigestUtils.sha512(dateTime.toString()));
            mac.update(DigestUtils.sha512(UUID.randomUUID().toString()));
            return DigestUtils.sha512Hex(mac.doFinal());
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String buildRefreshToken(UUID userId, LocalDateTime dateTime) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(key.getBytes(), "RawBytes"));
            mac.update(DigestUtils.sha512(dateTime.toString()));
            mac.update(DigestUtils.sha512(userId.toString()));
            mac.update(DigestUtils.sha512(UUID.randomUUID().toString()));
            return DigestUtils.sha512Hex(mac.doFinal());
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }
}
