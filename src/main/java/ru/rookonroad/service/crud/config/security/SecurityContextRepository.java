package ru.rookonroad.service.crud.config.security;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.repository.SessionRepo;

import java.util.Objects;

@Component
@AllArgsConstructor
public class SecurityContextRepository implements ServerSecurityContextRepository {

    private final SessionRepo sessionRepo;

    @Override
    public Mono<Void> save(ServerWebExchange serverWebExchange, SecurityContext securityContext) {
        return null;
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange serverWebExchange) {
        SecurityContext sc = SecurityContextHolder.getContext();
        Authentication auth = new AuthToken();
        if (serverWebExchange.getRequest().getHeaders().get("Authorization") == null) {
            auth.setAuthenticated(false);
            sc.setAuthentication(auth);
            return Mono.just(sc);
        }
        return sessionRepo.findSessionByTokens_AccessToken(
                Objects.requireNonNull(serverWebExchange.getRequest().getHeaders().get("Authorization")).get(0))
                .map(session -> {
                    auth.setAuthenticated(true);
                    sc.setAuthentication(auth);
                    return sc;
                }).doOnError(error -> {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                });
    }
}
