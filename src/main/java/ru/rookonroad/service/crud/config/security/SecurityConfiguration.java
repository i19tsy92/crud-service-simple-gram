package ru.rookonroad.service.crud.config.security;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@AllArgsConstructor
public class SecurityConfiguration {

    private final AuthManager authenticationManager;

    private final SecurityContextRepository contextRepository;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity security) {
        return security.httpBasic().disable()
                .formLogin().disable()
                .csrf().disable()
                .cors().disable()
                .authenticationManager(this.authenticationManager)
                .securityContextRepository(this.contextRepository)
                .authorizeExchange().pathMatchers("/auth").permitAll().and()
                .authorizeExchange().pathMatchers("/auth/**").permitAll().and()
                .authorizeExchange().anyExchange().authenticated().and()
                .build();
    }
}
