package ru.rookonroad.service.crud.config.security;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.repository.SessionRepo;

@Component
@AllArgsConstructor
public class AuthManager implements ReactiveAuthenticationManager {

    private final SessionRepo sessionRepo;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return null;
    }
}
