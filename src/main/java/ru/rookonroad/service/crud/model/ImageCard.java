package ru.rookonroad.service.crud.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@Document(collection = "ImageCard")
public class ImageCard {

    private String id;

    private String image;

    private UUID user;

    private Boolean isNew;
}
