package ru.rookonroad.service.crud.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tokens {

    private String accessToken;
    private String refreshToken;
    private Long expiresIn;
}
