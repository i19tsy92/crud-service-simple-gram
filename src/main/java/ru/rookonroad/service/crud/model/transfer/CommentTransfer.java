package ru.rookonroad.service.crud.model.transfer;

import lombok.Getter;
import lombok.Setter;
import ru.rookonroad.service.crud.model.Comment;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class CommentTransfer {

    private String message;
    private UUID user;
    private String username;
    private String imageCard;
    private String datetime;

    public CommentTransfer(Comment comment) {
        this.message = comment.getMessage();
        this.user = comment.getUser();
        this.username = comment.getUsername();
        this.imageCard = comment.getImageCard();
        this.datetime = comment.getDatetime().toString();
    }
}
