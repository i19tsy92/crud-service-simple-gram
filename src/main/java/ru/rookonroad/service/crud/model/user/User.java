package ru.rookonroad.service.crud.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Document("user")
@NoArgsConstructor
public class User {

    @Id
    private UUID id;

    private String username;

    @JsonIgnore
    private String hashPassword;

    private Tokens tokens;

    @JsonIgnore
    private List<String> downloaded;
}
