package ru.rookonroad.service.crud.model.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.rookonroad.service.crud.model.user.Tokens;
import ru.rookonroad.service.crud.utils.TokensUtils;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "session")
public class Session {

    @Id
    private String id;

    private UUID userId;

    private LocalDateTime dateTime;

    private Tokens tokens;

    public Session(UUID userId, LocalDateTime dateTime, Long expiresIn) {
        this.userId = userId;
        this.dateTime = dateTime;
        this.tokens = new Tokens(
                TokensUtils.buildAccessToken(userId, dateTime),
                TokensUtils.buildRefreshToken(userId, dateTime),
                expiresIn
        );
    }
}
