package ru.rookonroad.service.crud.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "messages")
public class Message {
    @Id
    private String id;

    private String message;

    private String user;

    private LocalDateTime dateTime;

    @JsonIgnore
    private String channelId;
}
