package ru.rookonroad.service.crud.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Client;
import ru.rookonroad.service.crud.service.ClientService;

@RestController
@RequestMapping("client")
public class ClientController {

    private final ClientService service;

    public ClientController(ClientService service) {
        this.service = service;
    }

    @PostMapping
    public Mono<Client> create(@RequestBody Client client) {
        return service.addNew(client);
    }
}
