package ru.rookonroad.service.crud.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Message;
import ru.rookonroad.service.crud.repository.ChatRepository;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/chat/{channel}")
@AllArgsConstructor
public class ChatController {

    private final ChatRepository chatRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Message> postChat(@RequestBody Message chatMessage, @PathVariable("channel") String channel) {
        chatMessage.setChannelId(channel);
        chatMessage.setDateTime(LocalDateTime.now());
        return chatRepository.save(chatMessage);
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Message> stream(@PathVariable("channel") String channel) {
        return chatRepository.findWithTailtableCursorByChannelId(channel);
    }


}
