package ru.rookonroad.service.crud.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.user.AuthData;
import ru.rookonroad.service.crud.model.user.Tokens;
import ru.rookonroad.service.crud.model.user.User;
import ru.rookonroad.service.crud.service.AuthService;

@RestController
@AllArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    @PutMapping
    public Mono<User> auth(@RequestBody AuthData authData) {
        return authService.auth(authData);
    }

    @PostMapping("/refresh")
    public Mono<Tokens> refresh(@RequestBody String refreshToken) {
        return authService.refresh(refreshToken);
    }
}
