package ru.rookonroad.service.crud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Comment;
import ru.rookonroad.service.crud.service.CommentService;

@RestController
@RequestMapping("/comment")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    public Mono<Comment> postComment(@RequestBody Comment comment) {
        return commentService.addComment(comment);
    }

    @GetMapping
    public Flux<Comment> getComments(@RequestParam String cardId) {
        return commentService.getAllComentsForCard(cardId);
    }
}
