package ru.rookonroad.service.crud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.ImageCard;
import ru.rookonroad.service.crud.service.CardService;

@RestController
@RequestMapping("card")
public class CardController {

    private final CardService cardService;

    public CardController(CardService service) {
        this.cardService = service;
    }

    @PostMapping
    public Mono<ImageCard> post(@RequestBody ImageCard card) {
        return cardService.postCard(card);
    }

    @GetMapping
    public Mono<ImageCard> getNext() {
        return cardService.getRandomCardList();
    }
}
