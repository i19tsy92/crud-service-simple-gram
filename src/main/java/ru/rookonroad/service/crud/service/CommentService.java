package ru.rookonroad.service.crud.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Comment;
import ru.rookonroad.service.crud.repository.CommentRepo;

@Service
public class CommentService {

    private final CommentRepo commentRepo;

    public CommentService(CommentRepo repo) {
        this.commentRepo = repo;
    }

    public Mono<Comment> addComment(Comment comment) {
        if (comment.getImageCard() == null || comment.getUser() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return commentRepo.save(comment);
    }

    public Flux<Comment> getAllComentsForCard(String cardId) {
        return commentRepo.findAllByImageCard(cardId);
    }

}
