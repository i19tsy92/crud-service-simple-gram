package ru.rookonroad.service.crud.service;

import org.springframework.data.mongodb.gridfs.ReactiveGridFsTemplate;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Image;
import ru.rookonroad.service.crud.repository.ImageRepo;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;


@Service
public class ImageService {

    private final ImageRepo imageRepo;

    private final ReactiveGridFsTemplate fsTemplate;

    public ImageService(ImageRepo imageRepo, ReactiveGridFsTemplate fsTemplate) {
        this.imageRepo = imageRepo;
        this.fsTemplate = fsTemplate;
    }

    public Flux<Void> getImageById(String id, ServerWebExchange exchange) {
        return fsTemplate.findOne(query(where("_id").is(id)))
                .log()
                .flatMap(fsTemplate::getResource)
                .flatMapMany(r -> exchange.getResponse().writeWith(r.getDownloadStream()));
    }

    public Mono<Map<String, String>> saveImage(Mono<FilePart> file) {
        Image image = new Image();
        return file.flatMap(part -> {
            image.setFilename(part.filename());
            return this.fsTemplate.store(part.content(), part.filename());
        }).map((id) -> {
            image.setId(id.toHexString());
            imageRepo.save(image);
            return new HashMap<String, String>() {{
                put("id", id.toHexString());
            }};
        });
    }
}
