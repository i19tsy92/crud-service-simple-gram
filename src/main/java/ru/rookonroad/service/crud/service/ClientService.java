package ru.rookonroad.service.crud.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.Client;
import ru.rookonroad.service.crud.repository.ClientRepo;

import java.util.UUID;

@Service
public class ClientService {

    private final ClientRepo clientRepo;

    public ClientService(ClientRepo repo) {
        this.clientRepo = repo;
    }

    public Mono<Client> addNew(Client client) {
        client.setId(UUID.randomUUID());
        return clientRepo.save(client);
    }

}
