package ru.rookonroad.service.crud.service;

import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.security.Session;
import ru.rookonroad.service.crud.model.user.AuthData;
import ru.rookonroad.service.crud.model.user.Tokens;
import ru.rookonroad.service.crud.model.user.User;
import ru.rookonroad.service.crud.repository.SessionRepo;
import ru.rookonroad.service.crud.repository.UserRepository;
import ru.rookonroad.service.crud.utils.TokensUtils;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {

    private final UserRepository userRepository;

    private final SessionRepo sessionRepo;

    private final Environment environment;

    public Mono<User> auth(AuthData authData) {
        if (authData.getUsername() == null || authData.getHashPassword() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return userRepository.findUserByUsername(authData.getUsername())
                .switchIfEmpty(createUser(authData))
                .map(user -> {
                    if (!user.getHashPassword().equals(authData.getHashPassword())) {
                        throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                    }
                    Session session = new Session(user.getId(), LocalDateTime.now(),
                            environment.getProperty("security.tokens.expire-time", Long.class));
                    user.setTokens(session.getTokens());
                    sessionRepo.save(session).subscribe();
                    return user;
                });
    }

    public Mono<User> createUser(AuthData authData) {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setHashPassword(authData.getHashPassword());
        user.setUsername(authData.getUsername());
        return userRepository.save(user);
    }

    public Mono<Tokens> refresh(String refreshToken) {
        return sessionRepo.findSessionByTokens_RefreshToken(refreshToken).map(session -> {
            session.setDateTime(LocalDateTime.now());
            session.setTokens(new Tokens(TokensUtils.buildAccessToken(session.getUserId(), session.getDateTime()),
                    TokensUtils.buildRefreshToken(session.getUserId(), session.getDateTime()),
                    environment.getProperty("security.tokens.expire-time", Long.class)));
            return session.getTokens();
        }).doOnError(error -> {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        });
    }
}
