package ru.rookonroad.service.crud.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import ru.rookonroad.service.crud.model.ImageCard;
import ru.rookonroad.service.crud.repository.CardRepo;

@Service
public class CardService {

    private final CardRepo cardRepo;

    public CardService(CardRepo repo) {
        this.cardRepo = repo;
    }

    public Mono<ImageCard> postCard(ImageCard card) {
        if (card.getImage() == null || card.getUser() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        card.setIsNew(true);
        return cardRepo.save(card);
    }

    public Mono<ImageCard> getRandomCardList() {
        return cardRepo.findAll().last();
    }
}
